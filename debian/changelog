libcrypt-random-source-perl (0.14-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.
  * Update standards version to 4.4.1, no changes needed.
  * Update standards version to 4.5.0, no changes needed.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on libtest-simple-perl and perl.

  [ gregor herrmann ]
  * Remove cdbs substvars from debian/control.
    Leftover from the cdbs → dh conversion,
    and break autopkgtests.
  * Update debian/upstream/metadata.
  * Bump debhelper-compat to 13.
  * Fix alternative test dependencies.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Sat, 09 Apr 2022 18:42:49 +0200

libcrypt-random-source-perl (0.14-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org.

  [ gregor herrmann ]
  * Update URLs from {search,www}.cpan.org to MetaCPAN.
  * Update GitHub URLs to use HTTPS.

  [ Jonas Smedegaard ]
  * Simplify rules.
    Stop build-depend on devscripts cdbs.
  * Update watch file:
    + Bump to file format 4.
    + Rewrite usage comment.
    + Use substitution strings.
  * Update git-buildpackage config: Filter any .git* file.
  * Stop build-depend on dh-buildinfo.
  * Relax to (build-)depend unversioned on libmoo-perl:
    Needed version satisfied even in oldstable.
  * Mark build-dependencies needed only for testsuite as such.
  * Declare compliance with Debian Policy 4.3.0.
  * Set Rules-Requires-Root: no.
  * Update copyright info:
    + Extend coverage of packaging.
    + Use https protocol in Format URL.
  * Drop obsolete lintian override regarding debhelper 9.
  * Tighten lintian overrides regarding License-Reference.
  * Build-depend on libtest-fatal-perl (not libtest-exception-perl).

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 22 Feb 2019 18:10:06 +0100

libcrypt-random-source-perl (0.12-1) unstable; urgency=medium

  * Team upload

  * New upstream version 0.12
  * drop insecure URL from debian/watch
  * add version to the (build) dependency on libmoo-perl
  * declare conformance with Policy 4.1.1

 -- Damyan Ivanov <dmn@debian.org>  Tue, 24 Oct 2017 13:37:16 +0000

libcrypt-random-source-perl (0.11-3) unstable; urgency=medium

  * Team upload

  * Use https in the Vcs-Git URL
  * Claim conformance with Policy 3.9.7
  * reverse alternatives in the Test::Simple build dependency

 -- Damyan Ivanov <dmn@debian.org>  Sun, 21 Feb 2016 19:24:35 +0000

libcrypt-random-source-perl (0.11-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Update Test::use::ok build dependency.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 30 Nov 2015 21:09:46 +0100

libcrypt-random-source-perl (0.11-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Bump prereq on namespace::clean and IO::File.
    + Fix test on older operating systems that define EAGAIN and
      EWOULDBLOCK as different values in errno.h.
    + Convert from Any::Moose to Moo.

  [ Jonas Smedegaard ]
  * Update copyright info:
    + List issue tracker as preferred upstream contact.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend copyright of packaging to cover current year.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.
  * Fix use CDBS snippet perl-build.mk (not perl-makemaker.mk).
  * Update package relations:
    + (Build-)depend on libmodule-runtime-perl libmoo-perl
      libtype-tiny-perl (not libany-moose-perl libclass-load-perl).
    + Build-depend on libmodule-build-tiny-perl: Needed by upstream
      build.
    + Tighten build-dependency on cdbs: Needed for Module::Build::Tiny
      builds.
  * Declare compliance with Debian Policy 3.9.6.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 09 Nov 2015 13:54:09 +0100

libcrypt-random-source-perl (0.08-2) unstable; urgency=medium

  * Team upload

  * Mark package as autopkgtest-able
  * Declare compliance with Debian Policy 3.9.6
  * Add explicit build dependency on libmodule-build-perl

 -- Damyan Ivanov <dmn@debian.org>  Tue, 09 Jun 2015 20:27:42 +0000

libcrypt-random-source-perl (0.08-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Update Vcs-Git URL to use canonical hostname (anonscm.debian.org).
  * Update Vcs-Browser URL to cgit web frontend.

  [ Jonas Smedegaard ]
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Fix use canonical Vcs-Git URI.
  * Update package relations:
    + (Build-)depend on libclass-load-perl.
    + Build-depend on libcpan-meta-perl: Optionally used in testsuite.
    + Relax to build-depend unversioned on cdbs: Needed version
      satisfied even in oldstable.
  * Update copyright info:
    + Add alternate git source.
    + Adjust coverage for main upstream author.
    + Extend coverage for packaging to include recent years.
    + Bump licensing for packaging to GPL-3+.
    + Fix use comment and license pseudo-sections to obey silly
      restrictions of copyright format 1.0.
  * Bump debhelper compatibility level to 8: Needed debhelper version
    satisfied even in oldstable.
  * Stop track or mangle experimental releases.
  * track md5sum of upstream tarball.
  * Improve watch file to use both metacpan.org URL (for fast response)
    and www.cpan.org/modules URL (for newest and older info).

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 27 Aug 2014 11:17:26 +0200

libcrypt-random-source-perl (0.07-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#676552.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 07 Jun 2012 20:49:42 +0200
